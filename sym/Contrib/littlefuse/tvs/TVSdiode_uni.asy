Version 4
SymbolType CELL
LINE Normal 0 16 -4 20
LINE Normal 32 16 36 12
LINE Normal 0 16 32 16
LINE Normal 0 40 32 40
LINE Normal 32 40 16 16
LINE Normal 0 40 16 16
LINE Normal 16 0 16 8
LINE Normal 16 40 16 48
LINE Normal 16 16 16 8
WINDOW 0 24 -2 Left 2
WINDOW 38 67 42 Center 0
WINDOW 1 41 25 Left 0
SYMATTR SpiceModel SLD8S12A
SYMATTR Prefix X
SYMATTR Description LittleFuse TVS
SYMATTR ModelFile Contrib\littlefuse\tvs\Littelfuse_TVS_Diode_SLD8SxxA_A_SPICE.lib
PIN 16 0 NONE 0
PINATTR PinName +
PINATTR SpiceOrder 2
PIN 16 48 NONE 0
PINATTR PinName -
PINATTR SpiceOrder 1
